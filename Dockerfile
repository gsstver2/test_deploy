FROM debian:jessie
RUN apt-get update && \
	apt-get install -y --no-install-recommends \
	python3-pip \
	supervisor \
	nginx
RUN mkdir /app
RUN mkdir /app/static
RUN mkdir /app/media
WORKDIR /app
ADD requirements.txt ./
RUN pip3 install -r requirements.txt
ADD src src
COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN python3 ./src/manage.py migrate --noinput
RUN python3 ./src/manage.py collectstatic --noinput
EXPOSE 80
CMD ["/usr/bin/supervisord", "--nodaemon"]
